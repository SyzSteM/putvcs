CREATE TABLE vcs_items_table
(
    absolute_path TEXT    NOT NULL PRIMARY KEY,
    backup_path   TEXT    NOT NULL UNIQUE,
    git_path      TEXT    NOT NULL UNIQUE,
    name          TEXT    NOT NULL,
    is_folder     INTEGER NOT NULL,
    creation_date TEXT    NOT NULL,
    last_changed  TEXT    NOT NULL
)
