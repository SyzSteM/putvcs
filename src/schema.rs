table! {
    vcs_items_table (absolute_path) {
        absolute_path -> Text,
        backup_path -> Text,
        git_path -> Text,
        name -> Text,
        is_folder -> Integer,
        creation_date -> Text,
        last_changed -> Text,
    }
}
