#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
// #![warn(clippy::cargo)]
#![allow(dead_code)]

#[macro_use]
extern crate diesel;

use std::ffi::OsString;
use std::os::unix;
use std::path::PathBuf;
use std::process::exit;
use std::{env, fs};

use chrono::{Local, NaiveDateTime};
use clap::Clap;
use diesel::{Connection, RunQueryDsl, SqliteConnection};
use dotenv::dotenv;
use fs_extra::dir::{copy, CopyOptions};
use log::{error, info, warn, LevelFilter};

use crate::models::VcsDbItem;
use crate::schema::vcs_items_table::dsl::vcs_items_table;

mod models;
mod schema;

// TODO: get GIT_PATH from config file or cli
const GIT_PATH: &str = "/home/timo/Downloads/tests/testdst";

const DATE_FORMAT: &str = "%Y-%m-%d %H:%M:%S";

/// Collection of important paths and properties of the item that is worked on
#[derive(Debug, Clone)]
struct VcsItem {
    /// File/folder name of the item under VCS
    name: OsString,
    /// Absolute original path of the item
    absolute_path: PathBuf,
    /// Path to the backup of the original item extended with *.bak*
    backup_path: PathBuf,
    /// Path to the original item in the VCS folder
    git_path: PathBuf,
    /// VcsItem struct type: File, Folder, Symlink
    path_type: PathType,
    // /// Item creation date
    creation_date: NaiveDateTime,
    // /// Items last changed date
    last_changed: NaiveDateTime,
}

impl VcsItem {
    /// Creates and returns a new and virtually empty Paths struct
    fn new() -> Self {
        Self {
            name: OsString::default(),
            absolute_path: PathBuf::default(),
            backup_path: PathBuf::default(),
            git_path: PathBuf::default(),
            path_type: PathType::File,
            creation_date: Local::now().naive_local(),
            last_changed: Local::now().naive_local(),
        }
    }

    fn from_db_table(db_table: Vec<VcsDbItem>) -> Vec<Self> {
        let mut vcs_items = Vec::new();

        for db_item in db_table {
            vcs_items.push(Self::from(&db_item));
        }

        vcs_items
    }

    fn into_db_item(self) -> VcsDbItem {
        VcsDbItem {
            absolute_path: String::from(
                self.absolute_path
                    .to_str()
                    .expect("Could not convert absolute path to String"),
            ),
            backup_path: String::from(
                self.backup_path
                    .to_str()
                    .expect("Could not convert backup path to String"),
            ),
            git_path: String::from(
                self.git_path
                    .to_str()
                    .expect("Could not convert git path to String"),
            ),
            name: String::from(
                self.name
                    .to_str()
                    .expect("Could not convert name to String"),
            ),
            is_folder: match self.path_type {
                PathType::File => 0,
                PathType::Folder => 1,
                PathType::Symlink => {
                    unreachable!("Impossible to add a symlink; should have been caught earlier")
                }
            },
            creation_date: Local::now().format(DATE_FORMAT).to_string(),
            last_changed: Local::now().format(DATE_FORMAT).to_string(),
        }
    }
}

/// Generates and returns a Paths struct, built from the input path
/// # Exits
/// - if path does not exist
/// - if path does not point to a folder or file
impl From<&PathBuf> for VcsItem {
    fn from(path: &PathBuf) -> Self {
        if !path.exists() {
            error!("Path {:?} does not exist!", path);
            exit(1);
        }

        // strip trailing slash ('/') from path
        let stripped_path = path
            .parent()
            .expect("Could not get parent directory from path")
            .join(path.file_name().expect("Could not get name from path"));

        let absolute_path = stripped_path
            .canonicalize()
            .expect("Failed to canonicalize path!");

        let name = absolute_path
            .file_name()
            .expect("Could not get name")
            .to_owned();

        let mut backup_path = absolute_path.clone();
        backup_path.set_extension("bak");

        let git_path = PathBuf::from(GIT_PATH).join(
            absolute_path
                .strip_prefix("/") // convert to relative path
                .expect("Could not strip input path"),
        );

        let path_metadata =
            fs::symlink_metadata(&stripped_path).expect("Failed to get path metadata!");

        let path_type;
        if path_metadata.is_file() {
            path_type = PathType::File;
        } else if path_metadata.is_dir() {
            path_type = PathType::Folder;
        } else if !path_metadata.is_dir() && !path_metadata.is_file() {
            path_type = PathType::Symlink;
        } else {
            unreachable!()
        }

        Self {
            name,
            absolute_path,
            backup_path,
            git_path,
            path_type,
            creation_date: Local::now().naive_local(),
            last_changed: Local::now().naive_local(),
        }
    }
}

impl From<&VcsDbItem> for VcsItem {
    fn from(db_item: &VcsDbItem) -> Self {
        let absolute_path = PathBuf::from(&db_item.absolute_path);

        if absolute_path.exists() {
            let path_type = match &db_item.is_folder {
                0 => PathType::File,
                1 => PathType::Folder,
                _ => unreachable!("DB values should always be 0 or 1"),
            };

            let creation_date =
                NaiveDateTime::parse_from_str(db_item.creation_date.as_str(), "%Y-%m-%d %H:%M:%S")
                    .expect("Could not parse creation date");
            let last_changed =
                NaiveDateTime::parse_from_str(db_item.last_changed.as_str(), "%Y-%m-%d %H:%M:%S")
                    .expect("Could not parse change date");

            Self {
                name: OsString::from(&db_item.name),
                absolute_path,
                backup_path: PathBuf::from(&db_item.backup_path),
                git_path: PathBuf::from(&db_item.git_path),
                path_type,
                creation_date,
                last_changed,
            }
        } else {
            error!("'{}' does not exist", absolute_path.display());
            todo!("perform full check for DB entry validation")
        }
    }
}

#[derive(Debug, Clone)]
enum PathType {
    File,
    Folder,
    Symlink,
}

/// A minimal CLI tool to enhance and automate your personal dotfiles VSC system.
#[derive(Debug, Clap)]
#[clap(version = "0.1", author = "Timo T. <timo.tabertshofer@gmail.com>")]
struct Opts {
    /// Level of verbosity; can be used multiple times
    #[clap(short, long, parse(from_occurrences))]
    verbose: i32,
    /// Several subcommands
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Debug, Clap)]
enum SubCommand {
    #[clap(version = "0.1", author = "Timo T. <timo.tabertshofer@gmail.com>")]
    Add(Add),
    Cleanup(Cleanup),
    Remove(Remove),
}

/// Add files to your VCS
#[derive(Debug, Clap)]
struct Add {
    /// Path to file/folder to put under your VCS
    #[clap(parse(from_os_str), required = true)]
    path: Vec<PathBuf>,
}

/// Cleanup backup files from your system
#[derive(Debug, Clap)]
struct Cleanup {}

/// Remove files from your VCS
#[derive(Debug, Clap)]
struct Remove {
    /// Path to file/folder to remove from your VCS
    #[clap(parse(from_os_str), required = true)]
    path: Vec<PathBuf>,
}

fn main() {
    env_logger::builder().filter_level(LevelFilter::Info).init();
    info!("starting up logger");

    let opts: Opts = Opts::parse();

    info!("{:?}", opts.subcmd);

    // TODO: implement actual verbosity logging
    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'putvcs -v -v -v' or 'putvcs -vvv' vs 'putvcs -v'
    match opts.verbose {
        0 => println!("No verbose info"),
        1 => println!("Some verbose info"),
        2 => println!("Tons of verbose info"),
        _ => println!("Don't be crazy"),
    }

    // DB TESTING
    let connection = establish_connection();

    match opts.subcmd {
        SubCommand::Add(add) => handle_add(&add, &connection),
        SubCommand::Cleanup(_) => handle_cleanup(&VcsItem::new(), &connection),
        SubCommand::Remove(remove) => handle_remove(&remove),
    }
}

fn establish_connection() -> SqliteConnection {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    SqliteConnection::establish(&database_url)
        .unwrap_or_else(|_| panic!("Error connection to {}", database_url))
}

fn handle_add(add: &Add, connection: &SqliteConnection) {
    for path_buf in &add.path {
        let paths = VcsItem::from(path_buf);

        println!("{:#?}", paths);

        match paths.path_type {
            PathType::File => add_file_to_vcs(&paths),
            PathType::Folder => add_folder_to_vcs(&paths, connection),
            PathType::Symlink => {
                error!("Path {:?} is not supported; symlinks can not be added to VCS for security reasons!", paths.absolute_path);
                exit(1);
            }
        }
    }
}

fn remove_path_from_fs(path: &PathBuf) {
    if path.is_file() {
        // if let Err(err) = fs::remove_file(&path) {
        //     error!("Could not remove file: {}; {}", path.display(), err);
        //     exit(1);
        // }
        info!("Sym remove of '{}'", path.display());
    } else if path.is_dir() {
        // if let Err(err) = fs::remove_dir_all(&path) {
        //     error!("Could not remove dir: {}; {}", path.display(), err);
        //     exit(1);
        // }
        info!("Sym remove of '{}'", path.display());
    } else {
        unreachable!("Trying to cleanup something that is neither file nor folder")
    }

    info!("Path: '{}' was removed from filesystem", path.display());
}

fn handle_cleanup(vcs_item: &VcsItem, connection: &SqliteConnection) {
    if vcs_item.backup_path.exists() {
        info!("Specific backup path exists; clean it up");
        info!("{:#?}", vcs_item);

        remove_path_from_fs(&vcs_item.backup_path);
    } else {
        info!("No specific backup path; cleanup everything");

        let items = VcsItem::from_db_table(
            vcs_items_table
                .load::<VcsDbItem>(connection)
                .expect("Error loading items"),
        );

        println!("{:#?}", items);

        for item in &items {
            if item.backup_path.exists() {
                remove_path_from_fs(&item.backup_path);
            } else {
                todo!("Handle invalid DB entry")
            }
        }
    }
}

fn handle_remove(remove: &Remove) {
    for path_buf in &remove.path {
        let paths = VcsItem::from(path_buf);

        println!("{:#?}", paths);

        if let PathType::Symlink = paths.path_type {
            let git_metadata =
                fs::symlink_metadata(paths.git_path).expect("Could not get git item metadata");
            if git_metadata.is_file() {
                remove_file_from_vcs();
            } else if git_metadata.is_dir() {
                remove_folder_from_vcs();
            } else {
                unreachable!()
            }
        } else {
            error!("Tried to remove some item not under VCS");
            todo!("Check if item is under VCS")
        }
    }
}

fn remove_file_from_vcs() {
    unimplemented!()
}

fn remove_folder_from_vcs() {
    unimplemented!()
}

fn add_file_to_vcs(paths: &VcsItem) {
    info!("{:#?}", paths);

    todo!()
}

fn add_folder_to_vcs(vcs_item: &VcsItem, connection: &SqliteConnection) {
    // VARIABLES
    let git_dest_dir = vcs_item
        .git_path
        .parent()
        .expect("Could not get parent of git_path");

    // DEBUG MESSAGES
    info!("{:#?}", vcs_item);
    info!("{:#?}", git_dest_dir);

    // ACTIONS
    fs::create_dir_all(&git_dest_dir).expect("Could not create git destination path");

    let dir_copy_opts = CopyOptions {
        overwrite: true,
        skip_exist: false,
        buffer_size: 0,
        copy_inside: true,
        content_only: false,
        depth: 0,
    };

    match copy(&vcs_item.absolute_path, &git_dest_dir, &dir_copy_opts) {
        Ok(_) => info!(
            "Recursive copy from {:?} to {:?} finished",
            vcs_item.absolute_path, vcs_item.git_path
        ),
        Err(err) => {
            error!("Could not copy folder recursively; {}", err);
            exit(1);
        }
    };

    fs::rename(&vcs_item.absolute_path, &vcs_item.backup_path).expect("Failed to rename folder");

    unix::fs::symlink(&vcs_item.git_path, &vcs_item.absolute_path)
        .expect("Failed to symlink folder");

    diesel::insert_into(vcs_items_table)
        .values(vcs_item.to_owned().into_db_item())
        .execute(connection)
        .unwrap_or_else(|_| panic!("Insertion into DB failed: {:?}", vcs_item));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        println!("{:?}", Local::now().format(DATE_FORMAT).to_string());
    }
}
