use crate::schema::vcs_items_table;
use crate::VcsItem;

#[derive(Queryable, Insertable, Debug)]
#[table_name = "vcs_items_table"]
pub struct VcsDbItem {
    pub absolute_path: String,
    pub backup_path: String,
    pub git_path: String,
    pub name: String,
    pub is_folder: i32,
    pub creation_date: String,
    pub last_changed: String,
}

impl VcsDbItem {
    fn from_item(vcs_item: &VcsItem) -> Self {
        Self {
            absolute_path: String::from(
                vcs_item
                    .absolute_path
                    .to_str()
                    .expect("Could not convert path to String"),
            ),
            backup_path: String::from(
                vcs_item
                    .backup_path
                    .to_str()
                    .expect("Could not convert path to String"),
            ),
            git_path: "".to_string(),
            name: "".to_string(),
            is_folder: 0,
            creation_date: "".to_string(),
            last_changed: "".to_string(),
        }
    }
}
