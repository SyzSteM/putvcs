# putvcs

A minimal CLI tool to enhance and automate your personal dotfiles VCS system.

Easily put your system configuration files directly into your version controlled dotfiles folder via file copy or
symlink. Automatically setup/restore your configuration files from your dotfiles repository (safety backups included).

## Features

- **add** files and folders to your VCS
- **setup** system files and folders from your VCS
- **cleanup** created backups
- **restore** files and folders from backup or your VCS
- **remove** files and folders from your VCS

## Roadmap/TODOs

**subcommands**, ***argument(s)***

- [ ] **add**
    - [ ] ***file(s)***
        - [ ] create backup *.bak*
        - [ ] create link
        - [ ] create and add database entry
    - [ ] ***folder(s)***
        - [x] copy contents recursively
        - [x] create backup *.bak*
        - [x] create link
        - [ ] create and add database entry
- [ ] **setup**
    - [ ] link/copy all files/folders from VCS to system
    - [ ] backup all existing files/folders to *.bak*, before overwriting
    - [ ] ***file(s)*** *TODO*
    - [ ] ***folder(s)*** *TODO*
- [ ] **cleanup**
    - [ ] remove all created backups *.bak*
    - [x] *file* reached indirectly
        - [x] remove the backup file *.bak* associated with *file*
    - [x] *folder* reached indirectly
        - [x] remove the backup folder *.bak* associated with *folder*
- [ ] **restore**
    - [ ] delete all existing symlinks or current files/folders
    - [ ] restore all original files/folders recursively from backup *.bak* or VCS
    - [ ] flag to switch between from *.bak* and from VCS
    - [ ] ***file(s)*** *TODO*
    - [ ] ***folder(s)*** *TODO*
- [ ] **remove**
    - [ ] ***file(s)***
        - [ ] replace link/copy of file with **restore** file from VCS
        - [ ] **cleanup** the backup file *.bak*
        - [ ] remove file from VCS
        - [ ] remove file entry from database
        - [ ] remove leftover empty folders
    - [ ] ***folder(s)***
        - [ ] replace link/copy of folder with **restore** folder from VCS
        - [ ] **cleanup** the backup folder *.bak*
        - [ ] remove folder recursively from VCS
        - [ ] remove folder entry from database
        - [ ] remove leftover empty folders
- [ ] **CLI**
    - [x] accept multiple paths as arguments
    - [ ] accept config file as argument or parse existing config file (personal VCS path, etc.)
    - [ ] verbosity level trigger flags
        - [x] accept multiple flags (-v, -vv, -vvv)
        - [ ] different terminal output based on verbosity level
- [ ] env_logger with different and useful logging levels *TODO: think about logging levels and trigger flags*
- [ ] SQLite database *TODO: think up database system, data structure, queries, etc.*
    - [ ] always check if item exists in the database, before trying to operate on it
- [ ] automatic handling of system (*copy*) vs. user (*soft link*) files/folders in all commands
    - [ ] systemd service and timer for periodical backup of system files
